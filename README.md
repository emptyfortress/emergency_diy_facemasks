# Emergency DIY facemask tutorial

Resources for making your own face mask in an emergency. Keep in mind I am not a doctor nor a medical professional, just a translator. These masks will **NOT** protect you as well as a proper medical one (the original design is intended for hospital support workers, not medical professions), but they're good for:

 - stopping **some** of your coughing. It does **not** absolve you from taking other measures to protect others from your cough/sneezes, like coughing in the crook of your elbow
 - preventing you from touching your face
 - contribute to public perception that protection measures should be taken seriously
 
It is recommended to wash them daily, and to be careful when removing them in case they've been exposed to pathogens.

You can find the tutorial in the "pdfs" folder, "source" has the source document in libreoffice format.

That being said, please share and distribute widely, new translations or forks of the project welcome.

The original tutorial is from the French hospital of Saint-Brieuc, my translation and this README are in the public domain.


# Other resources
If you don't have access to sewing equipment, you can try [this video tutorial](https://www.youtube.com/watch?v=grRqoHxPjaY) which uses a hot glue gun instead.

